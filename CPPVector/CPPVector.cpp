// CPPVector.cpp : コンソール アプリケーションのエントリ ポイントを定義します。
//

#include "stdafx.h"
#include <vector>

int _tmain(int argc, _TCHAR* argv[])
{
	// int値を格納する可変長配列の変数を定義する方法
	std::vector<int> v;

	// 要素の大きさを指定して変数を定義する方法
	//std::vector<int> v(10);

	// 要素の大きさと初期値を指定して変数を定義する方法
	//std::vector<int> v(10, 1);

	// C++11以降では初期化子リストにる変数を定義する方法
	// ただしVisual Studio2012では使用できませんでした。「
	//std::vector<int> v{1, 2, 3};

	// 値を末尾に追加します。
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);

	// 指定した位置に値を挿入します。
	// 要素数より大きい位置には挿入できません。
	// 例では"v.begin() + 4"は実行時エラーになります。
	v.insert(v.begin() + 3, 5);

	// 要素数を取得します。
	int size = (int)v.size();

	// 添字を指定して値を取得する方法
	for (int i = 0; i < size; ++i)
	{
		printf("%d", v[i]);
	}
	printf("\n");

	// 範囲for文を使った値の取得方法
	for (auto& value : v)
	{
		printf("%d", value);
	}
	printf("\n");

	// イテレータを使って値を取得する方法
	for (auto itr = v.begin(); itr != v.end(); ++itr)
	{
		printf("%d", *itr);
	}
	printf("\n");

	// 末尾の値を削除します。
	v.pop_back();
	for (auto& value : v)
	{
		printf("%d", value);
	}
	printf("\n");

	// 指定した位置の値を削除します。
	// 要素数の範囲外を指定すると実行時エラーになります。
	v.erase(v.begin() + 1);
	for (auto& value : v)
	{
		printf("%d", value);
	}
	printf("\n");

	return 0;
}

